package chapter2.item1;

import java.util.EnumSet;

/**
 * chapter2-item1
 * 정적 팩토리 메소드는 디자인 패턴에서의 팩토리 메소드와 다르다.
 * 클라이언트가 클래스의 인스턴스를 얻는 전통적인 수단은 public 생성자다.
 * 하지만 클래스는 생성자와 별도로 정적 팩토리 메소드를 제공할 수 있다. (클래스의 인스턴스를 반환하는 단순한 정적 메소드)
 * 
 * 한 클래스에 시그니처가 같은 생성자가 여러 개 필요할 것 같다면, 생성자를 정적 팩토리 메소드로 바꾸고 각각의 차이를 잘 드러내는 이름을 지어준다.
 * 
 * 장점
 * 1. 이름을 가질 수 있다.
 * - 생성자에 넘기는 매개변수와 생성자 자체만으로는 반환될 객체의 특성을 제대로 설명하지 못한다.
 * - 정적 팩토리는 이름만 잘 지으면 반환될 객체의 특성을 쉽게 묘사할 수 있다.
 * example) BigInteger(int, int, Random) 과 정적 팩토리 메소드인 BigInteger.probablePrime 중 어느 쪽이 값이 소수인 BigInteger를 반환한다는 의미를 갖는지
 * 
 * 2. 호출될 때마다 인스턴스를 새로 생성하지는 않아도 된다.
 * - 불변 클래스(immutable class : item 17번)는 인스턴스를 미리 만들어 놓거나 새로 생성한 인스턴스를 캐싱하여 재활용하는 식으로 불필요한 객체 생성을 피할 수 있다.
 * example) 예로 Boolean.valueOf(boolean) 메소드는 객체를 아예 생성하지 않는다.
 * - (생성 비용이 큰) 같은 객체가 자주 요청되는 상황이라면 성능을 올릴 수 있다.
 * - 플라이웨이트 패턴(Flyweight pattern)도 이와 비슷한 기법.
 * 	 인스턴스 통제는 플라이웨이트 패턴의 근간이 되며, 열거 타입(item 34)은 인스턴스가 하나만 만들어짐을 보장한다.
 * - 반복되는 요청에 같은 객체를 반환하는 식으로 정적 팩토리 방식의 클래스는 언제 어느 인스턴스를 살아 있게 할지를 철저히 통제할 수 있다.
 * 
 * 3. 반환 타입의 하위 타입 객체를 반환할 수 있는 능력이 있다.
 * - 반환할 객체의 클래스를 자유롭게 선택할 수 있게 하는 높은 유연성을 가진다.
 * - API를 만들 대 이 유연성을 응용하면 구현 클래스를 공개하지 않고도 그 객체를 반환할 수 있어 API를 작게 유지할 수 있다.
 * 	 이는 인터페이스를 정적 팩토리 메소드의 반환 타입으로 사용하는 인터페이스 기반 프레임워크(item 20)를 만드는 핵심 기술이기도 하다.
 * 
 * 4. 입력 매개변수에 따라 매번 다른 클래스의 객체를 반환할 수 있다.
 * - 반환 타입의 하위 타입이기만 하면 어떤 클래스의 객체를 반환하든 상관없다.
 * 	 심지어 다음 릴리즈에서는 또 다른 클래스의 객체를 반환해도 된다.
 * - 예를들어 EnumSet 클래스(item 36)는 public 생성자 없이 오직 정적 팩토리만 제공하는데, openJDK에서는 원소의 수에 따라 두 가지 하위 클래스 중 하나의 인스턴스를 반환한다.
 * 	 원소가 64개 이하면 원소들을 long 변수 하나로 관리하는 RegularEnumSet의 인스턴스를,
 * 	 원소가 65개 이상이면 long 배열로 관리하는 JumboEnumSet의 인스턴스를 반환한다.
 * 
 * 5. 정적 팩토리 메소드를 작성하는 시점에는 반환할 객체의 클래스가 존재하지 않아도 된다.
 * - 이런 유연함은 서비스 제공자 프레임워크를 만드는 근간이 된다.
 * - 대표적인 서비스 제공자 프레임워크로는 JDBC가 있다.
 * 	 서비스 제공자 프레임워크에서의 제공자(provider)는 서비스의 구현체다.
 * 	 이 구현체들을 클라이언트에 제공하는 역할을 프레임워크가 통제하여, 클라이언트를 구현체로부터 분리해준다.
 * - 서비스 제공자 프레임워크는 3개의 핵심 컴포넌트로 이뤄진다.
 * 	 구현체의 동작을 정의하는 서비스 인터페이스(service interface)
 * 	 제공자가 구현체를 등록할 때 사용하는 제공자 등록 API(provider registration API)
 * 	 클라이언트가 서비스의 인스턴스를 얻을 때 사용하는 서비스 접근 API(service access API)
 * 
 * 
 * 클라이언트는 서비스 접근 API를 사용할 때 원하는 구현체의 조건을 명시할 수 있다.
 * 조건을 명시하지 않으면 기본 구현체를 반환하거나 지원하는 구현체들을 하나씩 돌아가며 반환한다.
 * 이 서비스 접근 API가 바로 서비스 제공자 프레임워크의 근간이라고 한 유연한 정적 팩토리의 실체다.
 *
 */
public class StaticFactoryMethod {
	
	public static void main(String[] args) {
		System.out.println(StaticFactoryMethod.valueOf(false));
		
		// enum 의 인자를 set에 등록
		EnumSet<Color> colors = EnumSet.allOf(Color.class);
		EnumSet<Color> redAndBlue = EnumSet.of(Color.RED, Color.BLUE);
		
		
	}

	/**
	 * java.lang.Boolean
	 * boolean 기본 타입의 박싱 클래스인 Boolean에서 발췌한 예이다.
	 * 기본 타입인 boolean 값을 받아 Boolean 객체 참조로 변환해 준다.
	 */
	public static Boolean valueOf(boolean b) {
		return b ? Boolean.TRUE : Boolean.FALSE;
	}
	
	/**
	 * 기본 생성자와 정적 팩토리 메소드의 예
	 */
	String factoryMethodName;
	
	public StaticFactoryMethod(){
		
	}
	public StaticFactoryMethod(String factoryMethodName){
		this.factoryMethodName = factoryMethodName;
	}
	
	public static StaticFactoryMethod getFactoryMethodName(String factoryMethodName){
		return new StaticFactoryMethod("factoryMethodName");
	}
	
	/**
	 * Object 유연성에 대한 예제
	 * @param flag
	 * @return
	 */
	public static StaticFactoryMethod getObject(boolean flag){
		return flag ? new StaticFactoryMethod() : new StaticFactoryMethodChild();
	}
	
	static class StaticFactoryMethodChild extends StaticFactoryMethod {
		
	}
	
	enum Color {
		RED, GREEN, BLUE
	}
	
	/** 
	 * 구현체의 동작을 정의하는 서비스 인터페이스(service interface)
	 * 제공자가 구현체를 등록할 때 사용하는 제공자 등록 API(provider registration API)
	 * 클라이언트가 서비스의 인스턴스를 얻을 때 사용하는 서비스 접근 API(service access API)
	 * 예를 들면 JDBC 와 같다. (JDBC는 서비스 로더로 되어있지는 않다 java 6부터 적용된 개념)
	 * 필요로 하는 것으로 (객체 등) 공급한다는 개념.
	 */
	public static StaticFactoryMethod getMyFoo(){
		StaticFactoryMethod foo = new StaticFactoryMethod();
		
		// FQCN(Fully Qualified Class Name) : 클래스가 속한 패키지명을 모두 포함한 이름
		// 예를 들어 java.lang.String s = new java.lang.String(); 이 있다.
		
		// TODO 어떤 특정 약속되어 있는 텍스트 파일에서 구현체의 FQCN 을 읽어온다.
		// TODO FQCN 에 해당하는 인스턴스를 생성한다
		// TODO foo 변수를 해당 인스턴스를 가리키도록 수정한다.
		
		return foo;
	}

}
